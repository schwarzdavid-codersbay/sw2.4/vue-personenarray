import { defineStore } from 'pinia'

export const usePersonStore = defineStore('persons', {
  state: () => ({
    persons: []
  }),
  actions: {
    deletePerson(personToDelete) {
      const personIndex = this.persons.findIndex(person => person.firstName === personToDelete.firstName && person.lastName === personToDelete.lastName)
      if(personIndex >= 0) {
        this.persons.splice(personIndex, 1)
      }
    }
  }
})
